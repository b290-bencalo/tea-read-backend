const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoute = require("./routes/userRoute");
const postRoute = require("./routes/postRoute");

const app = express();

// Connect MongoDB Database

mongoose.connect(
  "mongodb+srv://admin:admin123@zuitt.sd0rdgy.mongodb.net/tea-read?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.once("open", () =>
  console.log("Now connected to MongoDB Atlas.")
);

// Middleware
// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes

app.use("/users", userRoute);
app.use("/posts", postRoute);

if (require.main === module) {
  app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
  });
}
module.exports = { app, mongoose };
