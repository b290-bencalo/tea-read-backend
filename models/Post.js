const mongoose = require("mongoose");

const postSchema = new mongoose.Schema({
  author: {
    type: String,
    required: [true, "Author is required"],
  },
  authorId: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, "Author ID is required"],
  },
  body: {
    type: String,
    required: [true, "Body is required"],
  },
  likes: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: () => new Date().toUTCString(),
  },
});

module.exports = mongoose.model("Post", postSchema);
