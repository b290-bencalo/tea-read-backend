const express = require("express");
const router = express.Router();
const auth = require("../auth");
const postController = require("../controllers/postController");

router.post("/add", auth.verify, (req, res) => {
  postController
    .addPost(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/active", (req, res) => {
  postController
    .retrieveActivePosts()
    .then((resultFromController) => res.send(resultFromController));
});

router.put("/:postId/update", auth.verify, (req, res) => {
  data = {
    authorId: auth.decode(req.headers.authorization).id,
    params: req.params,
    body: req.body,
  };
  postController
    .updatePost(data)
    .then((resultFromController) => res.send(resultFromController));
});

router.patch("/:postId/archive", auth.verify, (req, res) => {
  data = {
    authorId: auth.decode(req.headers.authorization).id,
    params: req.params,
  };
  postController
    .archivePost(data)
    .then((resultFromController) => res.send(resultFromController));
});

router.patch("/:postId/like", auth.verify, (req, res) => {
  data = {
    likerId: auth.decode(req.headers.authorization).id,
    params: req.params,
  };
  postController
    .likePost(data)
    .then((resultFromController) => res.send(resultFromController));
});

router.patch("/:postId/unlike", auth.verify, (req, res) => {
  data = {
    likerId: auth.decode(req.headers.authorization).id,
    params: req.params,
  };
  postController
    .unlikePost(data)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
