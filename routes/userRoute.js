const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");

// Register
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Login
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Get Specific User
router.get("/details", auth.verify, (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  userController
    .getUser(userId)
    .then((resultFromController) => res.send(resultFromController));
});

// Set as admin
router.patch("/setAdmin", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    userController
      .setAsAdmin(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

// Remove as Admin
router.patch("/removeAdmin", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    userController
      .removeAsAdmin(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

// Edit user details
router.put("/update", auth.verify, (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  userController
    .updateUser(userId, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Show all users

router.get("/", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    userController
      .getAllUsers()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

module.exports = router;
