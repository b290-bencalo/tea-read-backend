const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");

// Register User
module.exports.registerUser = (reqBody) => {
  return User.find({ email: reqBody.email })
    .then((result) => {
      if (result.length) {
        return false;
      } else {
        let newUser = new User({
          firstName: reqBody.firstName,
          lastName: reqBody.lastName,
          email: reqBody.email,
          password: bcrypt.hashSync(reqBody.password, 10),
        });

        return newUser
          .save()
          .then((user) => {
            return true;
          })
          .catch((err) => {
            return false;
          });
      }
    })
    .catch((err) => {
      return false;
    });
};

// Login User
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((result) => {
      if (result === null) {
        return false;
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          reqBody.password,
          result.password
        );
        if (isPasswordCorrect) {
          return { access: auth.createAccessToken(result) };
        } else {
          return false;
        }
      }
    })
    .catch((err) => {
      return false;
    });
};

// Get specific user
module.exports.getUser = (userId) => {
  return User.findById(userId)
    .then((result) => result)
    .catch((err) => {
      return false;
    });
};

// Set as admin

module.exports.setAsAdmin = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((user) => {
      if (user) {
        user.isAdmin = true;
        user.save();

        return true;
      } else {
        return false;
      }
    })
    .catch((err) => {
      return false;
    });
};

// Remove as admin
module.exports.removeAsAdmin = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((user) => {
      if (user) {
        user.isAdmin = false;
        user.save();
        return true;
      } else {
        return false;
      }
    })
    .catch((err) => {
      return false;
    });
};

// Update user
module.exports.updateUser = async (userId, reqBody) => {
  try {
    const updatedUser = {
      firstName: reqBody.firstName,
      lastName: reqBody.lastName,
      password: bcrypt.hashSync(reqBody.password, 10),
    };

    const user = await User.findByIdAndUpdate(userId, updatedUser);
    return updatedUser;
  } catch (err) {
    return false;
  }
};

module.exports.getAllUsers = () => {
  return User.find({})
    .then((user) => user)
    .catch((err) => {
      return false;
    });
};
