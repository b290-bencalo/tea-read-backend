const Post = require("../models/Post");

module.exports.addPost = (reqBody) => {
  const newPost = new Post({
    author: reqBody.author,
    authorId: reqBody.authorId,
    body: reqBody.body,
  });

  return newPost
    .save()
    .then((post) => post)
    .catch((err) => false);
};

module.exports.retrieveActivePosts = () => {
  return Post.find({ isActive: true })
    .then((post) => post)
    .catch((err) => false);
};

module.exports.updatePost = (data) => {
  return Post.findOne({ _id: data.params.postId })
    .then((result) => {
      if (result) {
        if (result.authorId.toString() === data.authorId.toString()) {
          const updatedPost = {
            body: data.body.body, // Assign the 'body' value directly
          };
          return Post.findByIdAndUpdate(data.params.postId, updatedPost, {
            new: true,
          })
            .then((post) => post)
            .catch((err) => {
              return false;
            });
        } else {
          return false;
        }
      } else {
        return false;
      }
    })
    .catch((error) => {
      return false;
    });
};

module.exports.archivePost = (data) => {
  return Post.findOne({ _id: data.params.postId }).then((result) => {
    if (result) {
      if (result.authorId.toString() === data.authorId.toString()) {
        return Post.findByIdAndUpdate(data.params.postId, { isActive: false })
          .then((post) => true)
          .catch((err) => false);
      }
    } else {
      return false;
    }
  });
};

module.exports.likePost = (data) => {
  return Post.findByIdAndUpdate(
    data.params.postId,
    {
      $push: { likes: data.likerId },
    },
    {
      new: true,
    }
  ).exec();
};

module.exports.unlikePost = (data) => {
  return Post.findByIdAndUpdate(
    data.params.postId,
    {
      $pull: { likes: data.likerId },
    },
    {
      new: true,
    }
  ).exec();
};
